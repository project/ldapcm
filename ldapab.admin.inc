<?php

/*******************************************************************************

  LDAP AddressBook for Drupal
  Copyright (C) 2010 Samuele ~redShadow~ Santi - Under GPL v3

********************************************************************************
  Administrative forms/pages for ldabab
*******************************************************************************/

/**
 * List all the defined directories
 * @return unknown_type
 */
function ldapab_admin_directories() {
  module_load_include('inc', 'ldapab', 'ldapab.ldap');
  $header = array(t("Name"), t("Host"), t("Bind DN"), t("Actions"));
  $rows = array();

  $directories = ldapab_get_all_directories();
  foreach ($directories as $directory) {
    $rows[] = array(
      l($directory->title, "admin/settings/ldapab/directories/". $directory->id),
      $directory->connection['host'] . ($directory->connection['port'] ? ":". $directory->connection['port'] : ""),
      $directory->connection['binddn'],
      l(t("Edit"), "admin/settings/ldapab/directories/". $directory->id ."/edit")
      .' '. l(t("Delete"), "admin/settings/ldapab/directories/". $directory->id ."/delete"),
    );
  }
  return theme('table', $header, $rows) . l(t("New directory"), "admin/settings/ldapab/directories/new");
}

/**
 * Directory editing form
 * @param unknown_type $form_state
 * @param unknown_type $dir_id
 * @return unknown_type
 */
function ldapab_admin_directory_edit_form($form_state, $dir_id = NULL) {
  module_load_include('inc', 'ldapab', 'ldapab.ldap');

  $breadcrumb = array(
    //l(t("Home"), '<front>'),
    l(t("Admin"), 'admin'),
    //l(t("Settings"), 'admin/settings'),
    l(t("LDAP AddressBook"), 'admin/settings/ldapab'),
    l(t("Directories"), 'admin/settings/ldapab/directories'),
  );

  if ($dir_id) {
    // Edit directory
    $directory = ldapab_get_directory($dir_id);
    $defaults = $directory;
    $breadcrumb[] = l($directory->title, 'admin/settings/ldapab/directories/'. $dir_id .'/edit');
  }
  else {
    $defaults = new stdClass();
    $defaults->connection = array(
      'host' => 'localhost',
      'port' => '389',
      'scope' => 'sub',
    );
    $breadcrumb[] = l(t("New directory"), 'admin/settings/ldapab/directories/new');
  }

  drupal_set_breadcrumb($breadcrumb);

  $form = array();
  $form['dir_id'] = array(
    '#type' => 'value',
    '#value' => $dir_id,
  );
  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t("Title"),
    '#default_value' => $defaults->title,
    '#required' => TRUE,
  );
  $form['connection'] = array(
    '#type' => 'fieldset',
    '#title' => t("Connection parameters"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );

  $form['connection']['binddn'] = array(
    '#type' => 'textfield',
    '#title' => t(""),
    '#default_value' => $defaults->connection['binddn'],
  );
//  version   LDAP version  3
//  starttls  TLS is started after connecting   false
//  options   Array of additional ldap options as key-value pairs   array()
//  filter  Default search filter (string or preferably Net_LDAP2_Filter object). See LDAP filters  (objectClass=*)
//  scope   , see Search  sub
  $form['connection']['host'] = array(
    '#type' => 'textfield',
    '#title' => t("Host"),
    '#default_value' => $defaults->connection['host'],
    '#description' => t("LDAP server name to connect to. You can provide several hosts in an array in which case the hosts are tried from left to right"),
    '#required' => TRUE,
  );
  $form['connection']['port'] = array(
    '#type' => 'textfield',
    '#title' => t("Port"),
    '#default_value' => $defaults->connection['port'],
    '#description' => t("Port on the server"),
  );

  $form['connection']['binddn'] = array(
    '#type' => 'textfield',
    '#title' => t("Bind DN"),
    '#default_value' => $defaults->connection['binddn'],
    '#description' => t("The distinguished name to bind as (username)"),
    '#required' => TRUE,
  );
  $form['connection']['bindpw'] = array(
    '#type' => 'textfield',
    '#title' => t("Bind PW"),
    '#default_value' => $defaults->connection['bindpw'],
    '#description' => t("Password for the binddn")
  );
  $form['connection']['basedn'] = array(
    '#type' => 'textfield',
    '#title' => t("Base DN"),
    '#default_value' => $defaults->connection['basedn'],
    '#description' => t("LDAP base name (root directory)"),
    '#required' => TRUE,
  );
  $form['connection']['scope'] = array(
    '#type' => 'textfield',
    '#title' => t("Default Scope"),
    '#default_value' => $defaults->connection['scope'],
    '#description' => t("Default search scope (leave default: <code>sub</code> unless you know what are you doing)")
  );
  $form['connection']['filter'] = array(
    '#type' => 'textfield',
    '#title' => t("Default Filter"),
    '#default_value' => $defaults->connection['filter'],
    '#description' => t("Default search filter (leave blank if unsure)"),
  );

  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t("Generic options"),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
    '#tree' => TRUE,
  );
  $form['options']['searchbase'] = array(
    '#type' => 'textfield',
    '#title' => t("SearchBase"),
    '#default_value' => $defaults->options['searchbase'],
  );
  $form['options']['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t("Fields Configuration"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  // TODO here, add the form for field-specific configuration
  $form['options']['theming'] = array(
    '#type' => 'fieldset',
    '#title' => t("Theming options"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );
  $form['options']['theming']['list'] = array(
    '#type' => 'fieldset',
    '#title' => t("List view theming"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => "<div>empty</div>",
  );
  $form['options']['theming']['entry'] = array(
    '#type' => 'fieldset',
    '#title' => t("Entry view theming"),
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#value' => "<div>empty</div>",
  );
  $form['description'] = array(
    '#type' => 'textarea',
    '#title' => t("Description"),
    '#description' => t("Optional description of the LDAP directory"),
    '#default_value' => $defaults->description,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Save"),
  );
  return $form;
}

/**
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function ldapab_admin_directory_edit_form_validate($form, &$form_state) {
}

/**
 *
 * @param unknown_type $form
 * @param unknown_type $form_state
 * @return unknown_type
 */
function ldapab_admin_directory_edit_form_submit($form, &$form_state) {
  // Process the form submit
  $dir_id = $form_state['values']['dir_id'];

  $directory = new stdClass();
  $directory->title = $form_state['values']['title'];
  $directory->connection = serialize($form_state['values']['connection']);
  $directory->options = serialize($form_state['values']['options']);
  $directory->description = $form_state['values']['description'];

  if ($dir_id) {
    //update
    $directory->id = $dir_id;
    drupal_write_record('ldapab_directories', $directory, array('id'));
    drupal_set_message(t("Updated LDAP directoy: !link", array('!link' => l($directory->title, 'ldap-addressbook/'. $directory->id))));
  }
  else {
    //insert
    drupal_write_record('ldapab_directories', $directory);
    drupal_set_message(t("Added new LDAP directoy: !link", array('!link' => l($directory->title, 'ldap-addressbook/'. $directory->id))));
  }
}

