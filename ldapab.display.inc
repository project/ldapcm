<?php
/*******************************************************************************

  LDAP AddressBook for Drupal
  Copyright (C) 2010 Samuele ~redShadow~ Santi - Under GPL v3

********************************************************************************
  Displaying functions

  Search path description:
    ldap-addressbook/<dir_id>/<filter>
      _GET['order'] -> the ordering filter(s)
    _GET['viewtype'] -> the theming template to use

*******************************************************************************/


/**
 * LDAP AddressBook search form.
 *
 * Parameters to perform the search are read from the drupal path, and
 * from _GET. The form submits via-post to a function that cleans/rewrites
 * the URL then redirects to this page.
 *
 * The form also performes a search, then stores the results as serialized
 * array in a 'value' field -> $form['results'].
 * The theming function should read them and render a table below the form.
 *
 * TODO add support for pager (paged query + themed pager)
 * TODO put full-text search fields definition on directry configuration
 * TODO create a way to define custom fields / widgets / etc.
 *
 * @param array $form_state Passed by Drupal form constructor
 * @param int $dir_id The id of the LDAP directory
 * @param string $fulltext Text to be searched on a list of specified fields
 * @return array The form structure to be themed
 */
function ldapab_search_form($form_state, $dir_id = NULL, $fulltext = NULL) {
//  dpm("form_state");
//  dpm($form_state);
//  dpm("_GET");
//  dpm($_GET);

  module_load_include('inc', 'ldapab', 'ldapab.ldap');
  if (!user_access('ldapab query addressbook')) {
    drupal_access_denied();
    return;
  }

  // -- load directory id/object -----------------------------------------------
  $dir_id = (int)$dir_id;
  $directory = ldapab_get_directory($dir_id);
  if (!$directory) {
    drupal_set_message(t("<strong>WARNING!</strong> Directory specified by url doesn't exist!"), 'warning');
    $directory = new stdClass();
  }
  else {
    drupal_set_title(t("Search directory @name", array('@name' => $directory->title)));
  }

  // -- build filters ----------------------------------------------------------
  // Parse the filters from GET
  $filters_val = $_GET['filters'];
  $ldap_filter = ldabap_create_filter_from_form($filters_val, 'and');


  // -- set the breadcrumb -----------------------------------------------------
  drupal_set_breadcrumb(array(
    l(t("Home"), '<front>'),
    l(t("Addressbook"), 'ldap-addressbook'),
    l($directory->title, 'ldap-addressbook/'. $dir_id),
  ));

  // initialize the form object
  $form = array('#method' => 'GET');

  // -- Administrative links on top of the form --------------------------------
  $form['top_links'] = array(
    '#weight' => -15,
  );
  if ($dir_id && user_access("ldapab manage directories")) {
    $form['top_links'][] = array(
      '#value' => l(t("Show directory"), "admin/settings/ldapab/directories/". $dir_id),
    );
    $form['top_links'][] = array(
      '#value' => l(t("Edit directory"), "admin/settings/ldapab/directories/". $dir_id ."/edit"),
    );
  }

  // -- Create search form -----------------------------------------------------
  $form['search_form_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t("Search"),
    '#collapsible' => TRUE,
    '#collapsed' => is_null($filter) ? FALSE : TRUE,
  );

  // Directory selection field (if no directory specified by URL)
  $form['search_form_wrapper']['dir_id_wrapper'] = array(
    '#type' => 'fieldset',
    '#title' => t("Directory selection"),
    '#collapsible' => TRUE,
    '#collapsed' => is_null($directory) ? FALSE : TRUE,
    '#weight' => -10,
  );
  $form['search_form_wrapper']['dir_id_wrapper']['dir_id'] = array(
    '#type' => 'select',
    '#title' => t("Directory"),
    '#options' => array(),
    '#description' => t("Select the directory on wich to perform the query"),
    '#default_value' => $dir_id,
  );
  $dirs = ldapab_get_all_directories();
  foreach ($dirs as $id => $dir) {
    $form['dir_id_wrapper']['dir_id']['#options'][$id] = $dir->title;
  }

  // Fields used to set filters
  $search_fields = ldapab_get_searchable_fields();

  // Available filter types
  $filter_types = ldapab_get_filter_types();

  // Full text search in the directory
  $form['search_form_wrapper']['fulltext_search'] = array(
    '#type' => 'fieldset',
    '#title' => t("Full Text Search"),
    '#collapsible' => TRUE,
    '#collapsed' => $fulltext ? FALSE : TRUE,
    '#weight' => -8,
  );
  $form['search_form_wrapper']['fulltext_search']['fulltext'] = array(
    '#type' => 'textfield',
    '#title' => t("Text to search"),
    '#description' => t("Insert here text to be searched in the !fields fields.<br /><strong>ATTENTION!</strong> This will disable the additional filters below.", array('!fields' => "cn,gn,sn,mail")),
    '#weight' => 0,
    '#default_value' => $fulltext,
  );
  $form['search_form_wrapper']['fulltext_search']['fulltext_fields'] = array(
    '#type' => 'textfield',
    '#title' => t("Full text search fields"),
    '#description' => t("Insert here the list of fields on which to perform the fulltext search.<br />Defaults to <code>!fields</code>.", array('!fields' => "cn,gn,sn,mail")),
    '#weight' => 1,
    '#default_value' => $_GET['fulltext_fields'] ? $_GET['fulltext_fields'] : "cn,gn,sn,mail",
  );

  // Filters "tree" field
  $form['search_form_wrapper']['filters'] = array(
    '#type' => 'fieldset',
    '#title' => t("Advanced filters"),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => $fulltext ? TRUE : FALSE,
    '#weight' => -2,
  );

  foreach ($search_fields as $name => $label) {
    $form['search_form_wrapper']['filters'][$name] = array(
      '#type' => 'fieldset',
      '#title' => $label,
      '#collapsible' => TRUE,
    );
    $form['search_form_wrapper']['filters'][$name]['attr_name'] = array(
      '#type' => 'value',
      '#value' => $name,
      '#default_value' => $filters_val[$name]['attr_name'],
    );
    $form['search_form_wrapper']['filters'][$name]['attr_label'] = array(
      '#type' => 'value',
      '#value' => $label,
    );
    $form['search_form_wrapper']['filters'][$name]['match'] = array(
      '#type' => 'select',
      '#title' => t("Match"),
      '#options' => $filter_types,
      '#default_value' => $filters_val[$name]['match'],
    );
    $form['search_form_wrapper']['filters'][$name]['value'] = array(
      '#type' => 'textfield',
      '#title' => t("Value"),
      '#default_value' => $filters_val[$name]['value'],
    );
  }


  $form['search_form_wrapper']['submit'] = array('#type' => 'submit', '#value' => t("Search"), '#weight' => 20);

  // TODO Search entries and store them here in a value
  if ($filter) {
    $form['results'] = array(
      '#type' => 'fieldset',
      '#title' => t("Search results"),
      '#value' => "<div>no search performed</div>",
      '#weight' => 20,
      '#collapsible' => TRUE,
    );

    // search
    $search = ldapab_search_entries($dir_id, $ldap_filters);

    // append entries to results
    if ($entries) {
      $entries = $search->sorted(array('sn', 'givenName'), SORT_ASC);

      $header = array('uid', 'Name', 'Surname', 'E-Mail', t("Actions"));
      $rows = array();
      foreach ($entries as $dn => $entry) {
      $rows[] = array(
        //$entry->dn(),
        $entry->exists('uid') ? $entry->getValue('uid', 'single') : $entry->dn(),
        $entry->exists('givenName') ? $entry->getValue('givenName', 'single') : "---",
        $entry->exists('sn') ? $entry->getValue('sn', 'single') : "---",
        $entry->exists('mail') ? theme('item_list', $entry->getValue('mail', 'all')) : "---",
        l(t("Show"), 'ldap-addressbook/'. $dir_id .'/'. urlencode($entry->dn()))
          .' '. l(t("Edit"), 'ldap-addressbook/'. $dir_id .'/'. urlencode($entry->dn()) .'/edit'),
        );
      }

      $form['results']['#value'] = theme('table', $header, $rows);
    }

    $form['debug'] = array(
      '#type' => 'fieldset',
      '#title' => t("Debug"),
      '#weight' => 25,
      '#collapsible' => TRUE,
      '#collapsed' => FALSE,
      '#value' => "<div><strong>Search filter:</strong> $filter</div>",
    );
  }
  return $form;
}

/**
 * Search submit handler
 * @param array $form
 * @param array $form_state
 * @return void
 * TODO call some function from ldapab.ldap.inc to build filters!
 */
function ldapab_search_form_submit($form, &$form_state) {
  if (!user_access('ldapab query addressbook')) {
    drupal_access_denied();
    return;
  }

  //$ldap = ldapab_get_connection();
  include_once "PEAR.php";
  include_once "Net/LDAP2.php";
  $filters = array();
  //dpm($form_state['values']['filters']);

  // For each filter specified by form, parse results and create
  // a Net_LDAP2_Filter to be used to generate the filter string.
  foreach ($form_state['values']['filters'] as $fv_filter) {
    if ($fv_filter['match']) {
      $newfilter = Net_LDAP2_Filter::create($fv_filter['attr_name'], $fv_filter['match'], $fv_filter['value']);
      if (PEAR::isError($filters[0])) {
        drupal_set_message($filters[0]->getMessage(), 'error');
      }
      else {
        $filters[] = $newfilter;
      }
    }
  }

  if (count($filters) > 1) {
    $filter = Net_LDAP2_Filter::combine('and', $filters);
  }
  elseif (count($filters) == 1) {
    $filter = $filters[0];
  }
  else {
    drupal_set_message(t("No filters specified. You must specify at least one to perform search.", 'warning'));
    return;
  }

  if (PEAR::isError($filter)) {
    drupal_set_message($filter->getMessage(), 'error');
    return;
  }

  // Get the directory id from the form
  $dir_id = (int)$form_state['values']['dir_id'];

  // Redirect to the search page, with filter set
  $form_state['redirect'] = "ldap-addressbook/$dir_id/". urlencode($filter->asString());
}

/**
 * Themeize the search form
 * @param array $form
 * @return string
 */
function theme_ldapab_search_form($form) {
  $header = NULL;
  $rows = array();

  $toplinks = array();
  foreach (element_children($form['top_links']) as $tl) {
    $toplinks[] = drupal_render($form['top_links'][$tl]);
  }
  $form['top_links'] = array(
    '#weight' => -15,
    '#value' => implode(" | ", $toplinks)
  );

  foreach (element_children($form['search_form_wrapper']['filters']) as $name) {
    unset($form['search_form_wrapper']['filters'][$name]['match']['#title']);
    unset($form['search_form_wrapper']['filters'][$name]['value']['#title']);
    $rows[] = array(
      array('data' => $form['search_form_wrapper']['filters'][$name]['attr_label']['#value'], 'header' => TRUE),
      drupal_render($form['search_form_wrapper']['filters'][$name]['match']),
      drupal_render($form['search_form_wrapper']['filters'][$name]['value']),
    );
    unset($form['search_form_wrapper']['filters'][$name]);
  }

  // TODO here, themeize the entries

  $form['search_form_wrapper']['filters']['#value'] = theme('table', $header, $rows);
  return drupal_render($form);
}


/**
 * List available directories
 * @return string
 */
function ldapab_page_listdirs() {
  module_load_include('inc', 'ldapab', 'ldapab.ldap');
  $directories = ldapab_get_all_directories();
  $header = array(t("Name"), t("Host"), t("Bind DN"), t("Actions"));
  $rows = array();
  foreach ($directories as $id => $dir) {
    $rows[] = array(
      l($dir->title, 'ldap-addressbook/'. $dir->id),
      $dir->connection['host'],
      $dir->connection['binddn'],
      l(t("Query"), 'ldap-addressbook/'. $dir->id),
    );
  }
  return theme('table', $header, $rows);
}

