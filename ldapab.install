<?php

/*******************************************************************************

  LDAP AddressBook for Drupal
  Copyright (C) 2010 Samuele ~redShadow~ Santi - Under GPL v3

********************************************************************************

  Installation functions

    - hook_requirements: check for PEAR and PEAR::Net_LDAP2 requirements
    - hook_schema: defines the schema for the tables:
      - ldapab_directories: list of configured directories to search on
        - id: serial
        - title: descriptive title
        - connection: (serialized php) directory connection parameters
        - options: (serialized php) misc options
        - description: descriptive (optional) text
    - hook_install: install the schema
    - hook_uninstall: uninstall the schema

*******************************************************************************/

/**
 * Implementation of hook_requirements()
 * @param string $phase
 * @return array
 */
function ldapab_requirements($phase) {
  $requirements = array();
  if (@include_once 'PEAR.php') {
    $description = t("PEAR Framework successfully loaded.");
    $severity = REQUIREMENT_OK;
    $value = "unknown version (TODO)";
  }
  else {
    $description = t("<a href='http://pear.php.net'>PEAR Framework</a> not found.");
    $severity = REQUIREMENT_ERROR;
    $value = "N/A";
  }

  $requirements['ldapab pear'] = array(
    'title' => t('LDAP AddressBook: PEAR framework'),
    'description' => $description,
    'severity' => $severity,
    'value' => $value,
  );

  if (@include_once 'Net/LDAP2.php') {
    $description = t("PEAR Module Net_LDAP2 (Net/LDAP2.php) found and successfully loaded.");
    $severity = REQUIREMENT_OK;
    $value = NET_LDAP2_VERSION;
  }
  else {
    $description = t("<a href='http://pear.php.net/package/Net_LDAP2/'>PEAR Net_LDAP2</a> module not found. Cannot connect to LDAP.<br />To install it (on Debian), just run: <code>apt-get install php5-pear && pear install Net_LDAP2</code>.<br />You can also manually download the code and place the 'Net' directory somewhere in the PHP inclusion path.");
    $severity = REQUIREMENT_ERROR;
    $value = "N/A";
  }

  $requirements['ldapab pear_ldap'] = array(
    'title' => t('LDAP AddressBook: PEAR::Net_LDAP2 module'),
    'description' => $description,
    'severity' => $severity,
    'value' => $value,
  );

  return $requirements;
}

/**
 * Implementation of hook_schema()
 * @return array
 */
function ldapab_schema() {
  $schema = array();
  $schema['ldapab_directories'] = array(
    'description' => "Table to store LDAP search directories",
    'fields' => array(
      'id' => array('type' => 'serial', 'not null' => TRUE),
      'title' => array('type' => 'varchar', 'length' => 64, 'not null' => TRUE),
      'connection' => array('type' => 'text'),
      'options' => array('type' => 'text'),
      'description' => array('type' => 'text'),
    ),
    'primary key' => array('id'),
  );
  return $schema;
}

/**
 * Implementation of hook_install()
 * @return void
 */
function ldapab_install() {
  drupal_install_schema('ldapab');
}

/**
 * Implementation of hook_uninstall()
 * @return void
 */
function ldapab_uninstall() {
  drupal_uninstall_schema('ldapab');
}

