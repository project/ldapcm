<?php

/*******************************************************************************

  LDAP AddressBook for Drupal
  Copyright (C) 2010 Samuele ~redShadow~ Santi - Under GPL v3

********************************************************************************

  LDAP querying functions

  The PEAR classes should be called ONLY inside this file, to provide flexible
  APIs to connect to the LDAP directory.

*******************************************************************************/

/**
 * Get the list of defined directories
 * @return unknown_type
 */
function ldapab_get_all_directories() {
  $res = db_query("SELECT * FROM {ldapab_directories}");
  $rows = array();
  while ($row = db_fetch_object($res)) {
    $row->connection = unserialize($row->connection);
    $row->options = unserialize($row->options);
    $row->searchbase = $row->options['searchbase'];
    $rows[$row->id] = $row;
  }
  return $rows;
}

/**
 * Gets the connection parameters to a given directory
 * TODO make this query the database, once it is defined..
 * @param unknown_type $dir_id
 * @return unknown_type
 */
function ldapab_get_directory($dir_id) {
  $row = db_fetch_object(db_query_range("SELECT * FROM {ldapab_directories} WHERE id=%d", $dir_id, 0, 1));
  if ($row) {
    $row->connection = unserialize($row->connection);
    $row->options = unserialize($row->options);
    $row->searchbase = $row->options['searchbase'];
    return $row;
  }
  else {
    return FALSE;
  }
  $connections = ldapab_get_all_directories();
  if (isset($connections[$dir_id])) {
    return $connections[$dir_id];
  }
  else {
    return NULL;
  }
}

/**
 * Get LDAP connection for a given directory
 * @return mixed
 */
function ldapab_get_connection($dir_id) {
  include_once "PEAR.php";
  include_once "Net/LDAP2.php";
  // Connect to LDAP and fetch all the entries
  $config = ldapab_get_directory($dir_id);
  // Connecting using the configuration:
  $ldap = Net_LDAP2::connect($config->connection);
  if (PEAR::isError($ldap)) {
    drupal_set_message(t("Could not connect to LDAP-server: @message", array('@message' => $ldap->getMessage())), 'error');
    return FALSE;
  }
  return $ldap;
}

/**
 * List the available filter types in a name => description array
 * @return unknown_type
 */
function ldapab_get_filter_types() {
  return array(
    '' => "---",
    'equals' => t("Equals"),
    'begins' => t("Begins with"),
    'ends' => t("Ends with"),
    'contains' => t("Contains"),
    'present' => t("Is present"),
    'any' => t("Any value"),
    'greater' => t("Greater than"),
    'less' => t("Less than"),
    'greaterOrEqual' => t("Grater or equal"),
    'lessOrEqual' => t("Less or equal"),
    'approx' => t("Similar to"),
  );
}

/**
 * Get the list of fields to be included in the search form
 * @return unknown_type
 */
function ldapab_get_searchable_fields() {
  // TODO query the schemas to get available fields
  // TODO improve this by allowing custom-defined fields
  return array(
    // -- standard inetOrgPerson fields ----------------------------------------
    //'dn' => t("Distinguished Name"),
    //'cn' => t("Common Name"),
    //'displayName' => t("Display Name"),
    'givenName' => t("Name"),
    'sn' => t("Surname"),
    'o' => t("Organization"),
    //'ou' => t("Organization Unit"),
    'mail' => t("E-Mail"),
    'homePhone' => t("Home Phone"),
    'mobile' => t("Mobile Phone"),
    //'objectClass' => t("Object Class"),
    //'userPassword' => t("Password"),
    //'postalAddress' => t("Address"),
    //'postalCode' => t("Postal Code"),
    'l' => t("Location"),
    // -- fields from juridicPersonIt schema by 2V S.r.l. ----------------------
    //'CodiceFiscale' => t("Codice Fiscale"),
    //'PartitaIva' => t("Partita IVA"),
    // -------------------------------------------------------------------------
  );
}

/**
 * Parse a filter string
 * @param unknown_type $filter
 * @return unknown_type
 */
function ldapab_parse_filter_string($filter) {
  include_once "PEAR.php";
  include_once "Net/LDAP2.php";
  $result = Net_LDAP2_Filter::parse($filter);
  if (PEAR::isError($result)) {
    drupal_set_message(t("Could not connect to LDAP-server: @message", array('@message' => $ldap->getMessage())), 'error');
    return FALSE;
  }
  else {
    return $result;
  }
}

/**
 * Perform a search on the directory
 * @param unknown_type $dir_id
 * @param unknown_type $filter
 * @return unknown_type
 */
function ldapab_search_entries($dir_id, $filter) {
  $ldap = ldapab_get_connection($dir_id);
  $dir = ldapab_get_directory($dir_id);
  $searchbase = $dir->options['searchbase'];
  $options = array('scope' => 'sub');
  $search = $ldap->search($searchbase, $filter, $options);
  if (PEAR::isError($search)) {
    drupal_set_message($search->getMessage(), 'error');
    return FALSE;
  }
  else {
    return $search;
  }
}

/**
 * Performs a full-text search on selected fields
 * @param int $dir_id
 * @param string $text the text to search
 * @param array $filter
 * @return Net_LDAP2_Search object if success, or FALSE if failed.
 */
function ldapab_search_fulltext($dir_id, $text, $fields = array('cn', 'givenName', 'sn', 'mail')) {
  $ldap = ldapab_get_connection($dir_id);
  $dir = ldapab_get_directory($dir_id);
  $searchbase = $dir['searchbase'];
  $options = array('scope' => 'sub');

  $filters = array();
  foreach ($fields as $field) {
    $filters[] = array(
      'attr_name' => $field,
      'match' => 'contains',
      'value' => $text,
    );
  }
  $filter = ldabap_create_filter_from_form($filters);
  $search = $ldap->search($searchbase, $filter, $options);
  if (PEAR::isError($search)) {
    drupal_set_message($search->getMessage(), 'error');
    return FALSE;
  }
  else {
    return $search;
  }
}

/**
 * Create filters from an array
 * @param array $filters an array of filters from form values
 * @param string $operator the operator used to combine the filters
 * @return
 *  Net_LDAP2_Filter leaf/group object if filter creation was successful
 *  NULL if no filter where specified
 *  FALSE if error occurred while creating filters
 */
function ldabap_create_filter_from_form($filters, $operator = 'and') {
  // Array to contain all the Net_LDAP2_Filter classes
  $filters = array();
  foreach ($filters as $filter_def) {
    // If the 'match' operator is selected, process the filter
    if ($filter_def['match']) {
      $newfilter = Net_LDAP2_Filter::create($filter_def['attr_name'], $filter_def['match'], $filter_def['value']);
      if (PEAR::isError($newfilter)) {
        // Error.. skip the filter and set a message
        drupal_set_message($newfilter->getMessage(), 'error');
      }
      else {
        // Ok. Append the filter to the list
        $filters[] = $newfilter;
      }
    }
  }

  if (count($filters) > 1) {
    // We have some filters to combine
    $filter = Net_LDAP2_Filter::combine($operator, $filters);
  }
  elseif (count($filters) == 1) {
    // Just one filter.. directly use
    $filter = $filters[0];
  }
  else {
    // No filters specified.. return NULL
    return NULL;
  }

  // Check for errors..
  if (PEAR::isError($filter)) {
    drupal_set_message($filter->getMessage(), 'error');
    return FALSE;
  }
  else {
    return $filter;
  }
}
