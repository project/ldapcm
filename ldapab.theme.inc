<?php

/*******************************************************************************

  LDAP AddressBook for Drupal
  Copyright (C) 2010 Samuele ~redShadow~ Santi - Under GPL v3

*******************************************************************************/


/**
 * Theming functions for LDAP entries
 * @param unknown_type $entry
 * @return unknown_type
 */
function theme_ldap_contact_entry($entry) {
  $rows = array();

  $field_labels = array(
    'cn' => t("Common Name"),
    'displayName' => t("Display Name"),
    'givenName' => t("Name"),
    'gn' => t("Name"),
    'sn' => t("Surname"),
    'homePhone' => t("Home Phone"),
    'mail' => t("E-Mail"),
    'mobile' => t("Mobile Phone"),
    'objectClass' => t("Object Class"),
    'userPassword' => t("Password"),
    'postalAddress' => t("Address"),
    'postalCode' => t("Postal Code"),
    'l' => t("Location"),
    'o' => t("Organization"),
    'ou' => t("Organization Unit"),
    'CodiceFiscale' => t("Codice Fiscale"),
    'PartitaIva' => t("Partita IVA"),
    'dn' => t("Distinguished Name"),
  );

  $rows[] = array(
    array('data' => $field_labels['dn'], 'header' => TRUE),
    array('data' => "dn", 'header' => TRUE),
    array('data' => $entry->dn()),
  );

  foreach ($entry->attributes() as $attrname) {
    foreach ($entry->getValue($attrname, 'all') as $value) {
      $rows[] = array(
        array('data' => $field_labels[$attrname], 'header' => TRUE),
        array('data' => $attrname,),
        array('data' => $value),
      );
    }
  }

  return theme('table', array(), $rows);
}
